﻿
## 概述
1、描述：progressbutton是占位面积很小的自定义进度指示器。默认实现提供了一个pin进度按钮。
2、实现功能：
    a.自定义圆形进度条。
    b.实现根据Slider滑动更新自定义进度条进度。
    c.实现进度条下载扫描动画。

## 效果展示

<img src = "https://gitee.com/openharmony-tpc/progressbutton/raw/master/progressbutton.gif"  style = "zoom : 45%"/>

## 集成引用
方法一：
1、添加har包到libs文件夹内
2、在entry的gradle内添加如下代码：
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])
}

方法二：
1、配置中心仓：
repositories {
    mavenCentral()
}
2、在entry的gradle内添加如下代码：
dependencies {
    implementation 'io.openharmony.tpc.thirdlib:progressbutton:1.0.2'
}

## 使用方法

ProgressButton  progressButton = new ProgressButton(Context context,  AttrSet attrSet）;
        progressButton.setPinned(true);
        progressButton.setMax(100);
        progressButton.setProgress(0);
        progressButton.setCircleColor(0xFF00FFFF);
        progressButton.setProgressColor(0xFF00FFFF);
        progressButton.setInnerSize(96);
        progressButton.setPinnedElement(new PixelMapElement());
        progressButton.setUnpinnedElement(new PixelMapElement());
        progressButton.setShadowElement(new PixelMapElement());
        progressButton.setAnimationStripWidth(6);
        progressButton.setAnimationSpeed(1);
        progressButton.setAnimationDelay(50);

License
=======

    Copyright 2013 Prateek Srivastava
    Copyright 2012 Roman Nurik
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
       http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
