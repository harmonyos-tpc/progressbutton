/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.f2prateek.progressbutton;
import com.f2prateek.progressbutton.utils.AttrUtils;
import ohos.agp.components.AbsButton;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Checkbox;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.util.Locale;
import java.util.Optional;

/**
 * 自定义progressbutton形状
 */
public class ProgressButton extends Checkbox implements Element.OnChangeListener {
    private static final HiLogLabel LOG_LABEL = new HiLogLabel(HiLog.LOG_APP, 0x20221, "ProgressButton");
    private static final String PRO_PROGRESS = "progress";
    private static final String PRO_MAX = "max";
    private static final String PRO_ELEMENT_SIZE = "element_size";
    private static final String PRO_CIRCLE_COLOR = "circle_color";
    private static final String PRO_PROGRESS_COLOR = "progress_color";
    private static final String PRO_PINNED_ELEMENT = "pinned_element";
    private static final String PRO_UNPINNED_ELEMENT = "unpinned_element";
    private static final String PRO_SHADOW_ELEMENT = "shadow_element";
    private static final String PRO_INNER_SIZE = "inner_size";
    private static final String PRO_BACKGROUND = "background";
    private static final String PRO_PINNED = "isPinned";
    private static final String PRO_ANIMATING = "animating";
    private static final String PRO_ANIMATION_SPEED = "animation_speed";
    private static final String PRO_ANIMATION_DELAY = "animation_delay";
    private static final String PRO_ANIMATION_STRIP_WIDTH = "animationStripWidth";

    /* * The maximum progress. Defaults to 100. */
    private int mMax = 100;

    /* * The current progress. Defaults to 0. */
    private int mProgress = 0;

    /* * The drawable used as the shadow. */
    private PixelMapElement mShadowElement;

    /* * The drawable displayed when the user unpins an item. */
    private PixelMapElement mUnpinnedElement;

    /* * The drawable displayed when the user pins an item. */
    private PixelMapElement mPinnedElement;

    /* * The paint for the circle. */
    private Paint mCirclePaint;

    /* * True if the view is animating. Defaults to false. */
    private boolean mAnimating = false;

    /* * Animation speed. Defaults to 1. */
    private int mAnimationSpeed = 1;

    /* * Delay between animation frames. Defaults to 50. */
    private int mAnimationDelay = 50;

    /* * Width of the animation strip. Defaults to 6. */
    private int mAnimationStripWidth = 6;

    /* * Internal variable to track animation state. */
    private int mAnimationProgress = 0;

    /**
     * The paint to show the progress.
     *
     * @see #mProgress
     */
    private Rect mTempRect = new Rect();

    private RectFloat mTempRectF = new RectFloat();
    private int mElementSize;
    private int mInnerSize = 96;
    boolean mIsPinned;

    private Paint mProgressPaint;

    private EventHandler mAnimationHandler =
            new EventHandler(EventRunner.getMainEventRunner()) {
                @Override
                protected void processEvent(InnerEvent event) {
                    if (mAnimating) {
                        HiLog.info(LOG_LABEL, "processEvent: mAnimating：" + mAnimating);
                        invalidate();
                        mAnimationProgress += mAnimationSpeed;
                        if (mAnimationProgress > mMax) {
                            mAnimationProgress = mProgress;
                        }
                        mAnimationHandler.sendEvent(0, mAnimationDelay);
                    }
                }
            };

    public ProgressButton(Context context) {
        this(context, null);
    }

    public ProgressButton(Context context, AttrSet attrSet) {
        super(context, attrSet);
        HiLog.info(LOG_LABEL, "ProgressButton~~~~~~~~~~");
        init(context, attrSet);
    }

    public ProgressButton(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context, attrSet);
    }

    private Optional<PixelMap> getPixelMap(Context context, int id) {
        try {
            String path = context.getResourceManager().getMediaPath(id);
            RawFileEntry assetManager = context.getResourceManager().getRawFileEntry(path);
            ImageSource.SourceOptions options = new ImageSource.SourceOptions();
            options.formatHint = "image/png";
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            Resource asset = assetManager.openRawFile();
            ImageSource source = ImageSource.create(asset, options);
            return Optional.ofNullable(source.createPixelmap(decodingOptions));
        } catch (Exception e) {
            HiLog.info(LOG_LABEL, "getPixelMap error: " + e.getMessage());
        }
        return Optional.empty();
    }

    /**
     * Initialise the {@link ProgressButton}.
     *
     * @param context the application environment
     * @param attrs Attribute Set provided
     */
    private void init(Context context, AttrSet attrs) {
        HiLog.info(LOG_LABEL, "progress init~~~" + attrs);
        mProgress = AttrUtils.getIntFromAttr(attrs, PRO_PROGRESS, mProgress);
        mMax = AttrUtils.getIntFromAttr(attrs, PRO_MAX, mMax);
        try {
            int circleColorDef = getResourceManager().getElement(ResourceTable.Color_progress_default_circle_color).getColor();
            int mProgressColorDef = getResourceManager().getElement(ResourceTable.Color_progress_default_progress_color).getColor();
            int circleColor = AttrUtils.getColorFromAttr(attrs, PRO_CIRCLE_COLOR, circleColorDef);
            HiLog.info(LOG_LABEL, "circleColor-----------" + circleColor);
            int progressColor = AttrUtils.getColorFromAttr(attrs, PRO_PROGRESS_COLOR, mProgressColorDef);
            HiLog.info(LOG_LABEL, "progressColor ------------" + progressColor);
            PixelMapElement pinElement =
                    new PixelMapElement(
                            context.getResourceManager().getResource(ResourceTable.Media_pin_progress_pinned));
            mPinnedElement = (PixelMapElement) AttrUtils.getElementFromAttr(attrs, PRO_PINNED_ELEMENT, pinElement);
            mPinnedElement.setCallback(this);
            PixelMapElement unpinnedElement =
                    new PixelMapElement(getResourceManager().getResource(ResourceTable.Media_pin_progress_unpinned));
            mUnpinnedElement =
                    (PixelMapElement) AttrUtils.getElementFromAttr(attrs, PRO_UNPINNED_ELEMENT, unpinnedElement);
            mUnpinnedElement.setCallback(this);
            PixelMapElement shadowElement =
                    new PixelMapElement(getResourceManager().getResource(ResourceTable.Media_pin_progress_shadow));
            mShadowElement = (PixelMapElement) AttrUtils.getElementFromAttr(attrs, PRO_SHADOW_ELEMENT, shadowElement);
            mShadowElement.setCallback(this);
            int shadowPicWidth =
                    getPixelMap(context, ResourceTable.Media_pin_progress_shadow).get().getImageInfo().size.width;
            mElementSize = AttrUtils.getIntFromAttr(attrs, PRO_ELEMENT_SIZE, shadowPicWidth);
            mInnerSize = AttrUtils.getIntFromAttr(
                    attrs, PRO_INNER_SIZE, getResourceManager().getElement(ResourceTable.Integer_progress_inner_size).getInteger());
            HiLog.info(LOG_LABEL, "mInnerSize == " + mInnerSize);
            mIsPinned = AttrUtils.getBooleanFromAttr(attrs, PRO_PINNED, false);
            setChecked(mIsPinned);
            setBackground(AttrUtils.getElementFromAttr(attrs, PRO_BACKGROUND, null));
            mAnimating = AttrUtils.getBooleanFromAttr(attrs, PRO_ANIMATING, mAnimating);
            mAnimationSpeed = AttrUtils.getIntFromAttr(attrs, PRO_ANIMATION_SPEED, mAnimationSpeed);
            mAnimationDelay = AttrUtils.getIntFromAttr(attrs, PRO_ANIMATION_DELAY, mAnimationDelay);
            mAnimationStripWidth = AttrUtils.getIntFromAttr(attrs, PRO_ANIMATION_STRIP_WIDTH, mAnimationStripWidth);
            initPaint(circleColor, progressColor);
        } catch (Exception e) {
            HiLog.info(LOG_LABEL, e.getMessage());
        }
        addDrawTask(new ProgressDrawTask());
        if (mAnimating) {
            startAnimating();
        }
    }

    private void initPaint(int circleColor, int progressColor) {
        mCirclePaint = new Paint();
        mCirclePaint.setColor(new Color(circleColor));
        mCirclePaint.setAntiAlias(true);

        mProgressPaint = new Paint();
        mProgressPaint.setColor(new Color(progressColor));
        mProgressPaint.setAntiAlias(true);
    }

    class ProgressDrawTask implements DrawTask {
        @Override
        public void onDraw(Component component, Canvas canvas) {
            HiLog.info(LOG_LABEL, "onDraw:~~~~~~~~~~~~~~~~~~:" + mElementSize + "/" + mInnerSize + "/" + mProgress);
            mTempRect.set(0, 0, mElementSize, mElementSize);
            mTempRect.translateTo((getWidth() - mElementSize) / 2, (getHeight() - mElementSize) / 2);
            mShadowElement.setBounds(mTempRect);
            mShadowElement.drawToCanvas(canvas);

            mTempRectF = new RectFloat(0, 0, mInnerSize, mInnerSize);
            mTempRectF.translateTo((getWidth() - mInnerSize) / 2f, (getHeight() - mInnerSize) / 2f);
            canvas.drawArc(mTempRectF, new Arc(0, 360, true), mCirclePaint);
            canvas.drawArc(mTempRectF, new Arc(-90, 360f * mProgress / mMax, true), mProgressPaint);
            if (mAnimating) {
                canvas.drawArc(
                        mTempRectF,
                        new Arc(-90 + (360f * mAnimationProgress / mMax), mAnimationStripWidth, true),
                        mProgressPaint);
            }
            HiLog.info(LOG_LABEL, "isChecked : " + isChecked());
            Element iconDrawable = isChecked() ? mPinnedElement : mUnpinnedElement;
            iconDrawable.setBounds(mTempRect);
            iconDrawable.drawToCanvas(canvas);
        }
    }

    /**
     * Start animating the button.
     */
    public void startAnimating() {
        HiLog.info(LOG_LABEL, "startAnimating:" + mAnimating);
        if (!mAnimating) {
            mAnimating = true;
            mAnimationProgress = mProgress;
            mAnimationHandler.sendEvent(0);
        }
    }

    /**
     * Stop animating the button.
     */
    public void stopAnimating() {
        mAnimating = false;
        mAnimationProgress = mProgress;
        mAnimationHandler.removeEvent(0);
        invalidate();
    }

    /**
     * Returns the maximum progress value.
     *
     * @return progress button max
     */
    public int getMax() {
        return mMax;
    }

    /**
     * Sets the maximum progress value. Defaults to 100.
     *
     * @param max max progress
     */
    private void setMax(int max) {
        if (max <= 0 || max < mProgress) {
            throw new IllegalArgumentException(
                    String.format(Locale.ROOT, "Max (%d) must be > 0 and >= %d", max, mProgress));
        }
        mMax = max;
        invalidate();
    }

    /**
     * Returns the current progress from 0 to max.
     *
     * @return current progress
     */
    public int getProgress() {
        return mProgress;
    }

    /**
     * Sets the current progress (must be between 0 and max).
     *
     * @param progress current progress
     * @see #setMax(int)
     */
    public void setProgress(int progress) {
        if (progress > mMax || progress < 0) {
            throw new IllegalArgumentException(
                    String.format(Locale.ROOT, "Progress (%d) must be between %d and %d", progress, 0, mMax));
        }
        mProgress = progress;
        invalidate();
    }

    /**
     * Sets the current progress and maximum progress value, both of which
     * must be valid values.
     *
     * @param progress current progress
     * @param max      the max progress
     * @see #setMax(int)
     * @see #setProgress(int)
     */
    public void setProgressAndMax(int progress, int max) {
        if (progress > max || progress < 0) {
            throw new IllegalArgumentException(
                    String.format(Locale.ROOT, "Progress (%d) must be between %d and %d", progress, 0, max));
        } else if (max <= 0) {
            throw new IllegalArgumentException(String.format(Locale.ROOT, "Max (%d) must be > 0", max));
        }

        mProgress = progress;
        mMax = max;
        invalidate();
    }

    /**
     * Get the color used to display the progress level.
     *
     * @return progress color
     */
    public int getProgressColor() {
        return mProgressPaint.getColor().getValue();
    }

    /**
     * Sets the color used to display the progress level.
     *
     * @param progressColor progressColor
     */
    public void setProgressColor(int progressColor) {
        mProgressPaint.setColor(new Color(progressColor));
        invalidate();
    }

    /**
     * Get the color used to display the progress background.
     *
     * @return circle color
     */
    public int getCircleColor() {
        return mCirclePaint.getColor().getValue();
    }

    /**
     * Sets the color used to display the progress background.
     *
     * @param circleColor circle color value
     */
    public void setCircleColor(int circleColor) {
        mCirclePaint.setColor(new Color(circleColor));
        invalidate();
    }

    /**
     * Get the drawable that is displayed when the item is pinned.
     *
     * @return pinned element
     */
    public Element getPinnedElement() {
        return mPinnedElement;
    }

    /**
     * Set the drawable that is displayed when the item is pinned.
     *
     * @param pinnedElement pinned element
     */
    public void setPinnedElement(Element pinnedElement) {
        mPinnedElement = (PixelMapElement) pinnedElement;
        invalidate();
    }

    /**
     * Get the element that is displayed when the item is unpinned.
     *
     * @return unpinned element
     */
    public Element getUnpinnedELement() {
        return mUnpinnedElement;
    }

    /**
     * Set the element that is displayed when the item is unpinned.
     *
     * @param unpinnedElement unpinned element
     */
    public void setUnpinnedElement(Element unpinnedElement) {
        mUnpinnedElement = (PixelMapElement) unpinnedElement;
        invalidate();
    }

    /**
     * Get the drawable that is displayed as the shadow.
     *
     * @return get shadow element
     */
    public Element getShadowElement() {
        return mShadowElement;
    }

    /**
     * Set the drawable that is displayed as the shadow.
     *
     * @param context       context
     * @param shadowElement shadow element
     * @param resourceId    shadow resource id
     */
    public void setShadowElement(Context context, PixelMapElement shadowElement, int resourceId) {
        mShadowElement = shadowElement;
        mElementSize = getPixelMap(context, resourceId).get().getImageInfo().size.width;
        HiLog.info(LOG_LABEL, "mDrawableSize :" + mElementSize);
        invalidate();
    }

    /**
     * get inner size
     *
     * @return inner size
     */
    public int getInnerSize() {
        return mInnerSize;
    }

    /**
     * set inner size
     *
     * @param innerSize inner size
     */
    public void setInnerSize(int innerSize) {
        mInnerSize = innerSize;
        invalidate();
    }

    /**
     * Get whether the button is pinned or not.
     * Equivalent to {@link AbsButton#isChecked()}
     *
     * @return isChecked
     */
    public boolean isPinned() {
        return isChecked();
    }

    /**
     * Set whether the button is pinned or not.
     * Equivalent to {@link AbsButton#setChecked(boolean)}
     *
     * @param pinned true: set checked; false: set unchecked
     */
    public void setPinned(boolean pinned) {
        HiLog.info(LOG_LABEL, "pinned : " + pinned);
        setChecked(pinned);
        mIsPinned = pinned;
        invalidate();
    }

    /**
     * Returns true if the button is animating.
     *
     * @return is Animating
     */
    public boolean isAnimating() {
        return mAnimating;
    }

    /**
     * Get the animation speed.
     *
     * @return animation speed
     */
    public int getAnimationSpeed() {
        return mAnimationSpeed;
    }

    /**
     * Get the animation delay.
     *
     * @return animation delay
     */
    public int getAnimationDelay() {
        return mAnimationDelay;
    }

    /**
     * Get the width of the animation strip.
     *
     * @return Animation Strip Width
     */
    public int getAnimationStripWidth() {
        return mAnimationStripWidth;
    }

    /**
     * Set the animation speed. This speed controls what progress we jump by in the animation.
     *
     * @param animationSpeed speed
     */
    public void setAnimationSpeed(int animationSpeed) {
        mAnimationSpeed = animationSpeed;
    }

    /**
     * Set the delay of the animation. This controls the duration between each frame.
     *
     * @param animationDelay delay
     */
    public void setAnimationDelay(int animationDelay) {
        mAnimationDelay = animationDelay;
    }

    /**
     * Set the width of the animation strip.
     *
     * @param animationStripWidth the width of the animation strip
     */
    public void setAnimationStripWidth(int animationStripWidth) {
        mAnimationStripWidth = animationStripWidth;
    }

    @Override
    public void onChange(Element element) {
        HiLog.info(LOG_LABEL, "onChange" + element.isStateful());
    }

    @Override
    public void setBindStateChangedListener(BindStateChangedListener listener) {
        super.setBindStateChangedListener(listener);
        HiLog.info(LOG_LABEL, "setBindStateChangedListener");
    }
}
