﻿V2.1.1
已支持部分：
1、 自定义圆形进度条进度绘制（进度背景颜色、进度条进度颜色、选中状态变化等）；
2、 自定义圆形进度条扫描动画。
未支持部分：
1、当前api缺少类似onSaveInstanceState和onRestoreInstanceState接口，无法将进度条当前进度自动保存。

