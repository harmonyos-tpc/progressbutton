/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.f2prateek.progressbutton.samples.slice;

import com.f2prateek.progressbutton.ProgressButton;

import com.f2prateek.progressbutton.samples.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.IOException;
import java.util.Optional;

/** main ability slice */
public class MainAbilitySlice extends AbilitySlice implements Checkbox.CheckedStateChangedListener {
    private static final HiLogLabel LOG_LABEL = new HiLogLabel(HiLog.LOG_APP, 0x20221, "MainAbilitySlice");
    private ProgressButton mProgressButton1;
    private ProgressButton mProgressButton2;
    private ProgressButton mProgressButton3;
    private ProgressButton mProgressButton4;
    private ProgressButton mProgressButton5;
    private ProgressButton mProgressButton6;
    private ProgressButton mProgressButton7;
    private ProgressButton mProgressButton8;
    private ProgressButton mProgressButton9;
    private ProgressButton mProgressButton10;

    /** onStart
     * @param intent intent
     */
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        HiLog.info(LOG_LABEL, "onStart~~~~~~~~");
        mProgressButton1 = (ProgressButton) findComponentById(ResourceTable.Id_pin_progress_1);
        mProgressButton2 = (ProgressButton) findComponentById(ResourceTable.Id_pin_progress_2);
        mProgressButton3 = (ProgressButton) findComponentById(ResourceTable.Id_pin_progress_3);
        mProgressButton4 = (ProgressButton) findComponentById(ResourceTable.Id_pin_progress_4);
        mProgressButton9 = (ProgressButton) findComponentById(ResourceTable.Id_pin_progress_9);
        mProgressButton10 = (ProgressButton) findComponentById(ResourceTable.Id_pin_progress_10);

        mProgressButton1.setCheckedStateChangedListener(this);
        mProgressButton2.setCheckedStateChangedListener(this);
        mProgressButton3.setCheckedStateChangedListener(this);
        mProgressButton4.setCheckedStateChangedListener(this);
        mProgressButton9.setCheckedStateChangedListener(this);
        mProgressButton10.setCheckedStateChangedListener(this);

        Slider slider = (Slider) findComponentById(ResourceTable.Id_slider);
        slider.setValueChangedListener(
                new Slider.ValueChangedListener() {
                    @Override
                    public void onProgressUpdated(Slider slider, int progress, boolean bool) {
                        HiLog.info(LOG_LABEL, "onProgressUpdated~~~~~~~~~~~~~");
                        updateProgress(slider);
                    }

                    @Override
                    public void onTouchStart(Slider slider) {
                    }

                    @Override
                    public void onTouchEnd(Slider slider) {
                    }
                });
        initAddProgressButton();
        updateProgress(slider);

        final ToggleButton toggleButton = (ToggleButton) findComponentById(ResourceTable.Id_toggle_button);
        toggleButton.setCheckedStateChangedListener(this);
    }

    private void updateProgress(Slider slider) {
        updateProgressButton(mProgressButton1, slider);
        updateProgressButton(mProgressButton2, slider);
        updateProgressButton(mProgressButton3, slider);
        updateProgressButton(mProgressButton4, slider);
        updateProgressButton(mProgressButton5, slider);
        updateProgressButton(mProgressButton6, slider);
        updateProgressButton(mProgressButton7, slider);
        updateProgressButton(mProgressButton8, slider);
        updateProgressButton(mProgressButton9, slider);
        updateProgressButton(mProgressButton10, slider);
    }

    private void initAddProgressButton() {
        final DirectionalLayout container = (DirectionalLayout) findComponentById(ResourceTable.Id_container);
        mProgressButton5 = addProgressButton(container);
        mProgressButton6 = addProgressButton(container);
        mProgressButton6.setPinned(true);
        mProgressButton7 = addProgressButton(container);
        mProgressButton7.setPinned(true);
        mProgressButton7.setClickable(true);
        mProgressButton7.setFocusable(Component.FOCUS_ENABLE);
        mProgressButton8 = addProgressButton(container);
        try {
            mProgressButton8.setCircleColor(
                    getResourceManager().getElement(ResourceTable.Color_holo_green_dark).getColor());
            mProgressButton8.setProgressColor(
                    getResourceManager().getElement(ResourceTable.Color_holo_green_light).getColor());
        } catch (IOException | NotExistException | WrongTypeException e) {
           HiLog.info(LOG_LABEL, e.getMessage());
        }
        mProgressButton8.setClickable(true);
        mProgressButton8.setFocusable(Component.FOCUS_ENABLE);

        mProgressButton5.setCheckedStateChangedListener(this);
        mProgressButton6.setCheckedStateChangedListener(this);
        mProgressButton7.setCheckedStateChangedListener(this);
        mProgressButton8.setCheckedStateChangedListener(this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    /**
     * Helper method to update the progressButton's content description.
     *
     * @param button progress button
     */
    private void updatePinProgressContentDescription(ProgressButton button) {
        int progress = button.getProgress();
        if (progress <= 0) {
            button.setComponentDescription(
                    button.isChecked() ? "Pinned to device and waiting to download" : "Not downloaded");
        } else if (progress >= button.getMax()) {
            button.setComponentDescription(
                    button.isChecked() ? "Pinned to device and downloaded" : "Temporarily downloaded");
        } else {
            button.setComponentDescription(
                    button.isChecked() ? "Pinned to device and downloading" : "Temporarily downloading");
        }
    }

    /**
     * Helper method to update the progressButton's progress and it's
     * content description.
     *
     * @param progressButton progress button
     * @param slider         slider
     */
    private void updateProgressButton(ProgressButton progressButton, Slider slider) {
        progressButton.setProgress(slider.getProgress());
        updatePinProgressContentDescription(progressButton);
    }

    /**
     * Helper function that creates a new progress button, adds it to the given layout.
     * Returns a reference to the progress button for customization.
     *
     * @param container directional layout
     * @return progress button
     */
    private ProgressButton addProgressButton(DirectionalLayout container) {
        DirectionalLayout.LayoutConfig layoutConfig =
                new DirectionalLayout.LayoutConfig(
                        0, DirectionalLayout.LayoutConfig.MATCH_PARENT, LayoutAlignment.VERTICAL_CENTER, 1.0f);
        final ProgressButton progressButton =
                new ProgressButton(
                        this,
                        new AttrSet() {
                            @Override
                            public Optional<String> getStyle() {
                                return Optional.empty();
                            }

                            @Override
                            public int getLength() {
                                return 16;
                            }

                            @Override
                            public Optional<Attr> getAttr(int value) {
                                return Optional.empty();
                            }

                            @Override
                            public Optional<Attr> getAttr(String value) {
                                return Optional.empty();
                            }
                        });
        progressButton.setLayoutConfig(layoutConfig);
        container.addComponent(progressButton);
        return progressButton;
    }

    @Override
    public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
        if (absButton.getId() == ResourceTable.Id_toggle_button) {
            HiLog.info(LOG_LABEL, "toggleButton onCheckedChanged :" + isChecked + absButton.getId());
            if (isChecked) {
                // Here, I explicitly call startAnimating
                // If you want a  progress button that starts in an animating state,
                // use the `animating` attribute via xml and set it to true
                // You can control the animation speed, width of the strip that is displayed and
                // animation delay
                mProgressButton9.startAnimating();
                mProgressButton10.startAnimating();
            } else {
                mProgressButton9.stopAnimating();
                mProgressButton10.stopAnimating();
            }
        } else {
            HiLog.info(LOG_LABEL, "onCheckedChanged" + isChecked);
            ((ProgressButton) absButton).setPinned(isChecked);
            updatePinProgressContentDescription((ProgressButton) absButton);
        }
    }
}
